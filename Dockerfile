FROM alpine:3.7

MAINTAINER FearNixx Technik, <technik@fearnixx.de>

RUN apk update \
	&& apk upgrade \
	&& apk add --no-cache --update curl ca-certificates openssl git tar unzip bash gcc \
	&& adduser -D -h /home/container container
